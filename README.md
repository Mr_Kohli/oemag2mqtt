# OeMAG2MQTT
A simple webscraper that pulls the current energy price from the [OeMAG](https://www.oem-ag.at/de/home/) website and
publishes it to a MQTT Topic.

The following data is published to the configured topic (the standard is ```energy/OeMAG/```):

| Topic          | Description                                     |
|----------------|-------------------------------------------------|
| Request        | If set to 1, the data is updated and published. |
| Preis Datum    | The date, for which the price is valid.         |
| Export Preis   | The energy export price in €/kWh.               |
| Letztes Update | When the last update was pulled.                |

## Configuration
The Program is configured through the config.yaml file in the root directory:

| Name     | Description                                                                   | Required |
|----------|-------------------------------------------------------------------------------|----------|
| topic    | The topic the prices are published to. Defaults to ```energy/OeMAG/```        | No       |
| broker   | Address of the Broker.                                                        | Yes      |
| port     | The Port used to connect to the broker. Defaults to ```1883```                | No       |
| QOS      | The QOS Value used. Defaults to ```1```                                       | No       |
| username | Username the client uses to log into the broker. Leave empty if not required. | No       |
| password | Password the client uses to log into the broker. Leave empty if not required. | No       |
| hass     | If you want to use Home Assistant, set this to True so it can be discovered.  | No       |

### Example configuration
````yaml
broker: "broker.emqx.io"
port: "1883"
topic: "mqtt/oemag"
OQS: "0"
````
