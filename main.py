import os.path
import time

from modules import scraper

import logging
import yaml
import paho.mqtt.client as mqtt
from yaml.loader import SafeLoader

TOPIC = "energy/OeMAG/"
BROKER_ADDRESS = ""
PORT = 1883
QOS = 1
USERNAME = ""
PASSWORD = ""
HASS = False

oemag_data = []


def on_message(client, userdata, message):
    msg = str(message.payload.decode("utf-8"))
    try:
        msg = int(msg)
    except ValueError as e:
        logging.error("Invalid Message")

    if msg == 1:
        update_prices()
        publish_data(client)

        topic_request = TOPIC + "Request"
        client.publish(topic_request, "0", qos=QOS, retain=True)


def on_connect(client, userdata, flags, rc):
    match rc:
        case 0:
            logging.info("Connected to MQTT Broker: " + BROKER_ADDRESS)
            client.subscribe(TOPIC + "Request")
        case 2:
            logging.error("Connection refused – invalid client identifier")
            quit(-1)
        case 3:
            logging.error("Connection refused – server unavailable")
            quit(-1)
        case 4:
            logging.error("Connection refused – bad username or password")
            quit(-1)
        case 5:
            logging.error("Connection refused – not authorised")
            quit(-1)
        case _:
            logging.error("An unknown connection error occurred.")
            quit(-1)


def publish_data(client):
    # publish date
    topic_date = TOPIC + "Preis Datum"
    client.publish(topic_date, oemag_data[0], qos=QOS, retain=True)

    # publish price
    topic_price = TOPIC + "Export Preis"
    client.publish(topic_price, oemag_data[1], qos=QOS, retain=True)

    # publish update time
    topic_update_time = TOPIC + "Letztes Update"
    client.publish(topic_update_time, oemag_data[2], qos=QOS, retain=True)


def init_mqtt():
    if not os.path.isfile("config.yaml"):
        logging.error("config.yaml is missing!")
        quit(-1)

    with open("config.yaml", 'r') as f:
        config_data = yaml.load(f, Loader=SafeLoader)

        # Optional Configurations
        try:

            if config_data['topic'] is not None:
                global TOPIC
                TOPIC = config_data['topic']
        except KeyError:
            logging.error("No Topic defined")
        try:
            if config_data['port'] is not None:
                global PORT
                PORT = config_data['port']
        except KeyError:
            logging.error("No Port defined")
        try:
            if config_data['QOS'] is not None:
                global QOS
                QOS = config_data['QOS']
        except KeyError:
            logging.error("No QOS defined")
        try:
            if config_data['username'] is not None:
                global USERNAME
                USERNAME = config_data['username']
        except KeyError:
            logging.error("No Username defined")
        try:
            if config_data['password'] is not None:
                global PASSWORD
                PASSWORD = config_data['password']
        except KeyError:
            logging.error("No Password defined")
        try:
            if config_data['hass'] is not None:
                global HASS
                HASS = config_data['hass']
        except KeyError:
            logging.error("No HASS defined")
        # Required Configurations
        try:
            if config_data['broker'] is None:
                logging.error("Error: No Broker defined!")
                quit(-1)
            global BROKER_ADDRESS
            BROKER_ADDRESS = config_data['broker']
        except KeyError:
            logging.error("Error: No Broker defined!")
            quit(-1)

    client = mqtt.Client(client_id="OeMAG2MQTT")
    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set(username=USERNAME, password=PASSWORD)

    topic_online = TOPIC + "Request/available"
    #client.will_set(topic_online, "offline", qos=QOS, retain=True)

    client.connect(BROKER_ADDRESS, PORT)

    if HASS:
        home_assistant_discorvery(client)
        time.sleep(5)

    client.publish(topic_online, "online", qos=QOS, retain=True)

    publish_data(client)

    client.loop_forever()


def update_prices():
    global oemag_data
    oemag_data = scraper.get_price_and_date()
    logging.info("Prices Updated")


def home_assistant_discorvery(client):
    # Price sensor
    price_sensor_config_topic = "homeassistant/sensor/oemag/price/config"
    price_sensor_config = ("{\"name\": \"Export Preis\",\"unique_id\": \"oemag_export_price\",\"state_topic\": "
                           "\"energy/OeMAG/Export Preis\",\"device\": {\"identifiers\": [\"OeMAG\"],\"name\": "
                           "\"OeMAG\",\"manufacturer\": \"OeMAG Abwicklungsstelle für Ökostrom\"},\"value_template\": "
                           "\"{{ value }}\",\"state_class\": \"measurement\",\"icon\": \"mdi:currency-eur\","
                           "\"unit_of_measurement\": \"EUR/kWh\"}")
    client.publish(price_sensor_config_topic, price_sensor_config, qos=QOS, retain=True)

    # Price Month Sensor
    price_date_sensor_config_topic = "homeassistant/sensor/oemag/date/config"
    price_date_sensor_config = ("{\"name\": \"Preis Monat\",\"unique_id\": \"oemag_export_price_date\","
                                "\"state_topic\": \"energy/OeMAG/Preis Datum\",\"device\": {\"identifiers\": ["
                                "\"OeMAG\"],\"name\": \"OeMAG\",\"manufacturer\": \"OeMAG Abwicklungsstelle für "
                                "Ökostrom\"},\"value_template\": \"{{ value }}\",\"icon\": \"mdi:calendar-blank\"}")
    client.publish(price_date_sensor_config_topic, price_date_sensor_config, qos=QOS, retain=True)

    # Update Sensor
    update_sensor_config_topic = "homeassistant/sensor/oemag/update/config"
    update_sensor_config = ("{\"name\": \"Letztes Update\",\"unique_id\": \"oemag_export_price_update\","
                            "\"state_topic\": \"energy/OeMAG/Letztes Update\",\"device\": {\"identifiers\": ["
                            "\"OeMAG\"],\"name\": \"OeMAG\",\"manufacturer\": \"OeMAG Abwicklungsstelle für "
                            "Ökostrom\"},\"value_template\": \"{{ value | int | timestamp_custom('%H:%M:%S %d.%m.%Y') "
                            "}}\",\"icon\": \"mdi:update\"}")
    client.publish(update_sensor_config_topic, update_sensor_config, qos=QOS, retain=True)

    # Update Button
    update_button_config_topic = "homeassistant/button/oemag/config"
    update_button_config = ("{\"availability\": [{\"topic\": \"energy/OeMAG/Request/available\"}],"
                            "\"availability_mode\": \"latest\",\"command_topic\": \"energy/OeMAG/Request\",\"device\": {"
                            "\"identifiers\": [\"OeMAG\"],\"name\": \"OeMAG\",\"manufacturer\": \"OeMAG "
                            "Abwicklungsstelle für Ökostrom\"},\"entity_category\": \"config\",\"icon\": "
                            "\"mdi:update\",\"name\": \"Update\",\"object_id\": \"oemag_update_price\","
                            "\"payload_press\": 1,\"unique_id\": \"update_price\",\"device_class\": \"update\"}")
    client.publish(update_button_config_topic, update_button_config, qos=QOS, retain=True)

    logging.info("Home Assistant discovery information sent!")


def start_oemag2mqtt():
    # Init logging
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s: %(message)s')

    update_prices()
    init_mqtt()


if __name__ == "__main__":
    start_oemag2mqtt()
