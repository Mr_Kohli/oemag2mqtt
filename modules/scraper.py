import requests
from bs4 import BeautifulSoup
from datetime import datetime


def get_webpage():
    # Headers to mimic a browser visit
    web_headers = {'User-Agent': 'Mozilla/5.0'}

    # Returns a requests.models.Response object
    url = "https://www.oem-ag.at/de/start/"
    page = requests.get(url, headers=web_headers)

    return page


def get_price_and_date():
    # request web page
    page = get_webpage()

    # parse page through BS4
    soup = BeautifulSoup(page.text, 'html.parser')
    attrs = {'class': 'limiter'}

    # filter out the paragraph with the data in it
    raw_data = ""
    for data in soup.find_all('div', attrs=attrs):
        raw_data = data.find('p').text

    # filter out the date
    filter_1 = raw_data.split('Marktpreis für den ')
    filter_2 = filter_1[1].split(' ct/kWh')

    # put the data into their own variables to make it a bit more understandable
    date = filter_2[0].split(' beträgt ')[0]
    price = filter_2[0].split(' beträgt ')[1]

    # convert the price from a string to a float
    price = float(price.replace(",", "."))
    # convert price from ct/kWh to €/kWh
    price = round(price/100, 5)

    final_data = [date, price, str(datetime.now().timestamp())]
    return final_data

