FROM python:3.12-slim-bullseye
LABEL authors="Stefan Kohlbacher"

WORKDIR /usr/app/src

COPY main.py ./
COPY modules ./modules
COPY requirements.txt ./
COPY config.yaml ./

RUN ["python", "-m", "pip", "install", "--upgrade", "pip"]
RUN ["pip", "install", "-r", "requirements.txt"]

ENTRYPOINT ["python", "./main.py"]